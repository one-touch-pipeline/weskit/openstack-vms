<!--
  ~  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
  ~
  ~ Distributed under the MIT License. Full text at
  ~
  ~      https://gitlab.com/one-touch-pipeline/weskit/openstack-vms/-/blob/master/LICENSE
  ~
  ~ Authors: The WESkit Team
  -->

odcf-lsf-submission
===================

Requirements
------------

Configure the mounts for the LSF software stack and set up the configuration.

Role Variables
--------------

None. This is basically just mounting resources defined via a dictionary and does a bit of setup in `/etc`. That's it.

Dependencies
------------

None.


Example Playbook
----------------

```yaml
- name: WESkit Workers
  hosts: workers
  become: true
  roles:
    - name: odcf-lsf-submission
```


License
-------

MIT

Author Information
------------------

Philip R. Kensche
