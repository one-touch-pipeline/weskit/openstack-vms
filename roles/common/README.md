<!--
  ~  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
  ~
  ~ Distributed under the MIT License. Full text at
  ~
  ~      https://gitlab.com/one-touch-pipeline/weskit/openstack-vms/-/blob/master/LICENSE
  ~
  ~ Authors: The WESkit Team
  -->

common
======

General VM setup.

Requirements
------------

Tested with CentOS 7 DKFZ image in the ITCF cloud.

Role Variables
--------------

* `packages`: Package names to install


Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT
