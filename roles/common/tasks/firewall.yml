#  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#        https://gitlab.com/one-touch-pipeline/weskit/openstack-vms/-/blob/master/LICENSE
#
#  Authors: The WESkit Team

- name: 'Install ufw'
  become: yes
  ansible.builtin.package:
    name: ufw
    state: present

- name: Activate SSH in firewall
  ufw:
    rule: allow
    name: SSH
  tags:
    - firewall

- name: Enable firewall
  ufw:
    state: enabled
  tags:
    - firewall

- name: Open internal ports
  ufw:
    rule: allow
    direction: '{{ item[0].direction | default("in") }}'
    port: '{{ item[0].port }}'
    proto: '{{ item[0].protocol }}'
    from_ip: '{{ item[1] }}'
  with_nested:
    - "{{ firewall_open_ports_internal }}"
    - "{{ trusted_hosts_internal }}"
  tags:
    - firewall

- name: Open external ports
  ufw:
    rule: allow
    direction: '{{ item[0].direction | default("in") }}'
    port: '{{ item[0].port }}'
    proto: '{{ item[0].protocol }}'
    from_ip: '{{ item[1] }}'
  with_nested:
    - "{{ firewall_open_ports_external }}"
    - "{{ trusted_hosts_external }}"
  tags:
    - firewall
