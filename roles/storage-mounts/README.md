<!--
  ~  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
  ~
  ~ Distributed under the MIT License. Full text at
  ~
  ~      https://gitlab.com/one-touch-pipeline/weskit/openstack-vms/-/blob/master/LICENSE
  ~
  ~ Authors: The WESkit Team
  -->

storage-mounter
===============

Mount remote storage, e.g. from Isilon.

Requirements
------------

None.


Role Variables
--------------

  * `required_mounts`


Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT

