<!--
  ~  Copyright (c) 2023. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
  ~
  ~ Distributed under the MIT License. Full text at
  ~
  ~      https://gitlab.com/one-touch-pipeline/weskit/openstack-vms/-/blob/master/LICENSE
  ~
  ~ Authors: The WESkit Team
  -->

# Ansible Code for OpenStack VM Setup 

* Set up one or multiple VMs for running the WESkit docker-swarm on it.
* Docker swarm itself is not set up. Also WESkit itself is not installed.
* Access to local NFS or to remote NFS is possible, also Kerberos.

## Infrastructure

Ansible is used for setting up the infrastructure for running WESkit, not WESkit itself.

1. Set up VMs in the OpenStack This has to be done manually. No automation has been implemented, yet. Use CentOS 7 images. Choose a bigger image to be able to maintain container images.
2. Update the Ansible inventory in `ansible/playbook/inventory`
3. Test the connection
   ```shell
   ansible -i ./inventory -m ping all
   ```
4. Update the configurations in `ansible/playbook/group_vars` and `ansible/playbook/host_vars.
5. Start SSH-agent and add the key for login.
   ```bash
   eval $(ssh-agent -t 1h)
   ssh-add ~/.ssh/your_cloud_key
   ```
6. Set up a vault file using the `playbooks/credentials.enc.template`
   Use the `ansible-vault` program to create `credentials.enc`
   ```shell
   ansible-vault create playbooks/credentials.enc
   ```
7. Install external roles (locally) and collections (globally):
   ```shell
   ansible-galaxy install -p external-roles/ -r requirements.yml
   ansible-galaxy collection install -r requirements.yml
   ```
8. Run (e.g. with creating self-signed SSL certificates and uploading)
   ```shell
   ansible-playbook \
      -i ./inventory \
      -e @credentials.enc \
      --ask-vault-password \
      playbook.yaml \
      --extra-vars "ssl_certificate_selfsigned_create=true" \
      --extra-vars "ssl_certificate_create_selfsigned=true"
   ```

The playbook will take the following actions:

* Basic configuration of the DKFZ images (adusers, etc.)
* Firewall settings for SSH, NFS-access to Isilon, Docker Swarm communication
* Set up a Docker swarm on the nodes (as configured in the inventory)
* Set up some NFS export from the bastion-host as server to the workers (for testing).
* Set up a TIG stack for monitoring

This has never been tested on another cloud or with other than the DKFZ CentOS 7 images.

The `config/`, `x509/`, `data/` and `workflows/` directories need to be on shared filesystems. For testing put them into the `/local` directory on the bastion host.

## Dependencies

 * Ansible 4.2.0

### Roles & Collections

  * See `requirements.txt`

## To-Dos

  * Increase storage available for Docker images!
  * Put most user names into the vault
  * Replicated InfluxDB
  * Autodeploy WESkit in the Docker Swarm
  * InfluxDB: Set custom retention policy. E.g. maximally a year of data.
  * Get all.yaml:trusted_hosts_internal from the vault.
  * Ensure Redis is not accessible from outside the swarm (used to be necessary for ??? LSF from workers?)
